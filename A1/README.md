CS 275 Assignment 1: Dropbox Mover
=====

To build this you will need Temboo's java SDK and json.org's library in your build path.

You will also need to store your Temboo and Dropbox credentials in JSON format:

```javascript
//DropboxSecret.json
{
	"app_key":		"",	//Insert your app key 
	"app_secret":	""	//Insert your app secret
}
```

```javascript
//TembooSecret.json
{
	"user": "",		//Insert your Temboo username
	"app_name": "",	//Insert your Temboo app name
	"key": ""		//Insert your Temboo API key
}
```

I've commented the code heavily except for the Temboo choreos because there isn't much to say.