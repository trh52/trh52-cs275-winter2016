package main;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONException;
import org.json.JSONObject;

import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile.GetFileInputSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile.GetFileResultSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.UploadFile;
import com.temboo.Library.Dropbox.FilesAndMetadata.UploadFile.UploadFileInputSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.UploadFile.UploadFileResultSet;
import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth;
import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
import com.temboo.Library.Dropbox.OAuth.InitializeOAuth;
import com.temboo.Library.Dropbox.OAuth.InitializeOAuth.InitializeOAuthInputSet;
import com.temboo.Library.Dropbox.OAuth.InitializeOAuth.InitializeOAuthResultSet;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;

/**
 * Static class to run the program
 * 
 * @author Tobias Highfill
 *
 */
public class DropboxMover {
	
	/**
	 * JSONObject that carries all my super-top-secret Temboo API info
	 */
	private static final JSONObject TEMBOO_SECRET = Util.readJSON("TembooSecret.json");
	
	/**
	 * JSONObject that carries all my super-top-secret Dropbox API info
	 */
	private static final JSONObject DROPBOX_SECRET = Util.readJSON("DropboxSecret.json");
	
	/**
	 * The Temboo session all my Choreos use
	 */
	private static final TembooSession SESSION = initTemboo();
	
	/**
	 * My Dropbox app key
	 */
	private static final String DB_APPKEY = Util.getString(DROPBOX_SECRET, "app_key");
	
	/**
	 * My Dropbox app secret
	 */
	private static final String DB_APPSECRET = Util.getString(DROPBOX_SECRET, "app_secret");
	
	/**
	 * My access token for the user's Dropbox account
	 */
	private static String accessToken = null;
	
	/**
	 * My access token secret for the user's Dropbox account
	 */
	private static String accessTokenSecret = null;
	
	/**
	 * Initializes and returns the Temboo session
	 * @return The Temboo session
	 */
	private static TembooSession initTemboo(){
		try {
			return new TembooSession(
					TEMBOO_SECRET.getString("user"),
					TEMBOO_SECRET.getString("app_name"),
					TEMBOO_SECRET.getString("key"));
		} catch (TembooException e) {
			Util.handleException(e);
		} catch (JSONException e) {
			Util.handleException(e);
		}
		return null;
	}
	
	/**
	 * Handles the OAuth authorization
	 * 
	 * @throws TembooException 
	 * @throws URISyntaxException 
	 * @throws IOException 
	 */
	private static void authorize() throws TembooException, IOException, URISyntaxException{
		InitializeOAuth initChoreo = new InitializeOAuth(SESSION);
		InitializeOAuthInputSet initInputs = initChoreo.newInputSet();

		initInputs.set_DropboxAppKey(DB_APPKEY);
		initInputs.set_DropboxAppSecret(DB_APPSECRET);

		InitializeOAuthResultSet initResults = initChoreo.execute(initInputs);
		//Direct user to authorization page
		URL authURL = new URL(initResults.get_AuthorizationURL());
		Desktop.getDesktop().browse(authURL.toURI());

		FinalizeOAuth finalChoreo = new FinalizeOAuth(SESSION);
		FinalizeOAuthInputSet finalInputs = finalChoreo.newInputSet();

		finalInputs.set_DropboxAppKey(DB_APPKEY);
		finalInputs.set_DropboxAppSecret(DB_APPSECRET);
		finalInputs.set_OAuthTokenSecret(initResults.get_OAuthTokenSecret());
		finalInputs.set_CallbackID(initResults.get_CallbackID());

		FinalizeOAuthResultSet finalResults = finalChoreo.execute(finalInputs);
		//Extract results
		accessToken = finalResults.get_AccessToken();
		accessTokenSecret = finalResults.get_AccessTokenSecret();
	}
	
	/**
	 * Downloads the file from Dropbox and returns it's contents
	 * 
	 * @param src The filepath from root to the file in Dropbox
	 * @param base64Encode Set to true to download the file encoded as Base64, false otherwise
	 * @return The contents of the string, decoded
	 * @throws TembooException
	 */
	public static String download(String src, boolean base64Encode) throws TembooException{
		GetFile choreo = new GetFile(SESSION);
		GetFileInputSet inputs = choreo.newInputSet();
		
		//Set inputs
		inputs.set_AccessToken(accessToken);
		inputs.set_AccessTokenSecret(accessTokenSecret);
		inputs.set_AppKey(DB_APPKEY);
		inputs.set_AppSecret(DB_APPSECRET);
		inputs.set_Path(src);
		inputs.set_EncodeFileContent(base64Encode);

		GetFileResultSet results = choreo.execute(inputs);
		//Get the content
		String content = results.get_Response();
		//Decode the content, if necessary
		if(base64Encode){
			content = new String(Base64.decodeBase64(content));
		}
		return content;
	}
	
	/**
	 * <p>Downloads a file from Dropbox and saves it to a file on the system.</p>
	 * 
	 * <p>The destination can be either a file that may or may not exist or a directory that does.
	 * If the former the file will be saved directly, otherwise the file will be created in the given
	 * directory with the same name as the source.</p>
	 * 
	 * @param src The path from root to the file in Dropbox
	 * @param dest The target filename or directory
	 * @param base64Encode Set to true to download the file encoded as Base64, false otherwise
	 * @throws IOException
	 * @throws TembooException
	 */
	public static void downloadToFile(String src, String dest, boolean base64Encode) throws IOException, TembooException{
		//Download the content
		String content = download(src, base64Encode);
		
		//Save the content to a file
		File f = new File(dest);
		try {
			//Check if it's a directory
			if(f.isDirectory()){
				//Resolve the filename against this
				f = f.toPath().resolve(new File(src).getName()).toFile();
				//Create the parent directories if necessary
				f.getParentFile().mkdirs();
				//System.out.println("Updated to: " + f.getAbsolutePath());
			}
			//Create the file if it doesn't exist
			if(!f.exists()){
				//System.out.println("Creating: "+f.getAbsolutePath());
				f.createNewFile();
			}
			
			//Write the file
			PrintStream out = new PrintStream(f);
			out.print(content);
			out.flush();
			out.close();
		}catch (IOException e) {
			System.err.println("File: "+f.getAbsolutePath());
			throw e;
		}
	}
	
	/**
	 * Upload a file to Dropbox
	 * @param dest The folder to put it in
	 * @param fname The name of the file
	 * @param content The file contents
	 * @return The JSON response from Dropbox
	 * @throws TembooException
	 * @throws JSONException
	 */
	public static JSONObject upload(String dest, String fname, String content) throws TembooException, JSONException{
		//Encode the content
		content = new String(Base64.encodeBase64String(content.getBytes()));
		
		//System.out.println("Uploading content: " + content);
		
		UploadFile choreo = new UploadFile(SESSION);
		UploadFileInputSet inputs = choreo.newInputSet();

		//Set inputs
		inputs.set_AccessToken(accessToken);
		inputs.set_AccessTokenSecret(accessTokenSecret);
		inputs.set_AppKey(DB_APPKEY);
		inputs.set_AppSecret(DB_APPSECRET);
		inputs.set_Folder(dest);
		inputs.set_FileName(fname);
		inputs.set_FileContents(content);
		
		UploadFileResultSet results = choreo.execute(inputs);
		//Parse results into JSON and return
		return new JSONObject(results.get_Response());
	}

	/**
	 * The main function, does everything
	 * 
	 * @param args The command line arguments (ignored)
	 */
	public static void main(String[] args) {
		//Authorize
		try {
			authorize();
		} catch (Exception e1) {
			Util.handleException(e1);
		}
		
		//Set constants
		boolean base64 = true;
		String listDir = "move", listName = "__list";
		String listPath = listDir + '/' + listName;
		
		//Download list
		String list = null;
		try {
			list = download(listPath, base64);
		} catch (TembooException e) {
			Util.handleException(e);
		}
		//Trim whitespace
		list = list.trim();
		//Check if it's empty
		if(list.isEmpty()){
			System.out.println("List is empty!");
			System.exit(0); //We're done here
			return; // To be extra safe
		}
		
		//Parse the list and download the files
		String[] lines = list.split("\n");
		System.out.println("Executing:\n"+list);
		for(int i=0; i<lines.length; i++){
			String line = lines[i].trim();
			//Limits the split to the first space, separating the file and the destination
			String[] parts = line.split(" ", 2);
			//Trims the whitespace
			parts[0] = "move/" + parts[0].trim();
			parts[1] = parts[1].trim();
			
			//Download the file
			System.out.print(String.format("Downloading %s to %s...", parts[0], parts[1]));
			try {
				downloadToFile(parts[0], parts[1], base64);
				System.out.println("done!");
				lines[i] = ""; //Remove this entry in the list
			} catch (Exception e) {
				System.out.println("ERROR!");
				Util.handleException(e);
			}
		}
		
		//Rebuild the list
		list = "";
		for(String line : lines){
			list += line + "\n";
		}
		
		//Upload the new list
		try {
			upload(listDir, listName, list);
			System.out.println("List file updated!");
		} catch (Exception e) {
			Util.handleException(e);
		}
	}

}
