package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Static utility class
 * @author Tobias Highfill
 *
 */
public class Util {
	private Util(){}
	
	/**
	 * This function reads everything in a Reader and concatenates it into a String
	 * @param r The reader
	 * @return The contents
	 * @throws IOException
	 */
	public static String readAll(Reader r) throws IOException{
		BufferedReader br = new BufferedReader(r);
		StringBuilder res = new StringBuilder();
		String line = br.readLine();
		while(line != null){
			if(res.length() == 0){
				res.append(line);
			}else{
				res.append("\n"+line);
			}
			line = br.readLine();
		}
		return res.toString();
	}
	
	/**
	 * This function reads everything in an InputStream and concatenates it into a String
	 * @param in The InputStream
	 * @return The contents or null if there was a problem
	 */
	public static String readAll(InputStream in){
		try {
			return readAll(new InputStreamReader(in));
		} catch (IOException e) {
			handleException(e);
		}
		return null;
	}

	/**
	 * This function reads everything in a File and concatenates it into a String
	 * @param f The File
	 * @return The contents or null if there was a problem
	 */
	public static String readAll(File f){
		try {
			return readAll(new FileReader(f));
		} catch (Exception e) {
			handleException(e);
		}
		return null;
	}
	
	/**
	 * Convenience function to handle the parsing errors from converting to JSON
	 * 
	 * @param filename The JSON file to parse
	 * @return The JSONObject or null if there was an error
	 */
	public static JSONObject readJSON(String filename){
		try {
			return new JSONObject(Util.readAll(new File(filename)));
		} catch (JSONException e) {
			handleException(e);
			return null;
		}
	}
	
	/**
	 * Convenience function to catch the errors from looking for nonexistent keys
	 * @param obj The JSONObject to look in
	 * @param key The key to look for
	 * @return The content or null if there was an error
	 */
	public static String getString(JSONObject obj, String key){
		try {
			return obj.getString(key);
		} catch (JSONException e) {
			handleException(e);
		}
		return null;
	}
	
	/**
	 * Convenience function to handle errors for me.
	 * Prints the stack trace then exits with code -1.
	 * @param e The exception to handle
	 */
	public static void handleException(Exception e){
		e.printStackTrace();
		System.exit(-1);
	}

}
