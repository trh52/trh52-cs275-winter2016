CS 275 Lab 4 - Android and Web Services
=====
By: Tobias Highfill (trh52)

This lab was a little tricky because I had to decipher that whole
ListView/Adapter hack, but I think I've figured it out. First I have
to define a layout in XML titled something descriptive so I can find
it later. Then I create a subclass of ArrayAdapter using some type that
I want to be able to insert. In that class I override the getView()
method and convert the corresponding element into a view using the
layout I defined earlier. It's pretty terrible but I think I got it.

The rest of this app was fairly straight-forward. I used AsyncTask
subclasses to handle all the network calls such as location lookup,
forecast lookup, and downloading images. For the images I had to create
an event listener interface for when the forecast icon finished
downloading so I could update the UI.

For the database all I did was create one table with three columns: id,
fetchedAt, and jsonData. FetchedAt is an integer in which I store the
UNIX timestamp of the moment this forecast was stored. JsonData is text
that is the raw JSON of the forecast array. To get the most recent
forecast I simply run a select query, order by fetchedAt in descending
order, and limit 1.