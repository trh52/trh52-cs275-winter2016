package thighfill.l4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by mattress on 2/24/2016.
 */
public class Util {
    public static String readAll(InputStream in){
        StringBuilder res = null;
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String line = null;
        try {
            line = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        while(line != null){
            if(res == null){
                res = new StringBuilder(line);
            }else{
                res.append('\n').append(line);
            }
            try {
                line = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
                line = null;
            }
        }
        return res.toString();
    }
}
