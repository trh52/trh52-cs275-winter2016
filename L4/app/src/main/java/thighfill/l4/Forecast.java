package thighfill.l4;

import android.graphics.Bitmap;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Tobias Highfill
 */
public class Forecast {
    public String conditions;
    public int tempFahrenheit;
    public int tempCelsius;
    public int humidity;
    public Bitmap icon = null;
    public String icon_src = null;
    public String date;

    private List<IconUpdateListener> listenerList = new LinkedList<>();

    public Forecast(JSONObject forecast) throws JSONException {
        JSONObject temp = forecast.getJSONObject("temp");
        tempFahrenheit = temp.getInt("english");
        tempCelsius = temp.getInt("metric");
        conditions = forecast.getString("condition");
        humidity = forecast.getInt("humidity");
        date = forecast.getJSONObject("FCTTIME").getString("pretty");
        icon_src = forecast.getString("icon_url");
        URL url = null;
        try {
            url = new URL(icon_src);
            new ImageLoader(new Receiver<Bitmap>() {
                @Override
                public void receive(Bitmap bitmap) {
                    icon = bitmap;
                    for(IconUpdateListener listener : listenerList){
                        listener.iconUpdate(bitmap);
                    }
                }
            }).execute(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public void addIconUpdateListener(IconUpdateListener listener){
        listenerList.add(listener);
    }

    public interface IconUpdateListener{
        void iconUpdate(Bitmap icon);
    }
}
