package thighfill.l4;

import android.app.Activity;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author Tobias Highfill
 */
public class ForecastFetcher extends AsyncTask<String, Void, JSONArray>{

    final MainActivity parent;

    private JSONArray preFetch = null;

    public ForecastFetcher(MainActivity parent){
        this.parent = parent;
    }

    @Override
    protected void onPreExecute() {
        JSONArray array = parent.dataManager.getRecentForecast();
        if(array != null){
            parent.usingCacheToast();
            preFetch = array;
        }
    }

    @Override
    protected JSONArray doInBackground(String... params) {
        if(preFetch != null) {
            return preFetch;
        }
        URL url = null;
        try {
            url = new URL(String.format("http://api.wunderground.com/api/%s/hourly/q/%s.json", parent.apiKey, params[0]));
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
        String raw = null;
        try {
            raw = Util.readAll(url.openStream());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        try {
            JSONArray res = new JSONObject(raw).getJSONArray("hourly_forecast");
            parent.dataManager.saveForecast(res);
            return res;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(JSONArray hourly) {
        if(hourly == null){
            return;
        }
        for(int i=0; i<hourly.length(); i++){
            try {
                JSONObject forecast = hourly.getJSONObject(i);
                Forecast f = new Forecast(forecast);
                parent.forecastList.add(f);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
