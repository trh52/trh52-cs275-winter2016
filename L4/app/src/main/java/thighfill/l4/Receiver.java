package thighfill.l4;

/**
 * Created by mattress on 2/24/2016.
 */
public interface Receiver<E> {
    void receive(E e);
}
