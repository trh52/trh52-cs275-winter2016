package thighfill.l4;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Tobias Highfill
 */
public class ImageLoader extends AsyncTask<URL, Void, Bitmap> {

    private static final Map<URL, Bitmap> KNOWN = new HashMap<>();

    private final Receiver<Bitmap> receiver;

    public ImageLoader(Receiver<Bitmap> receiver){
        this.receiver = receiver;
    }

    @Override
    protected Bitmap doInBackground(URL... params) {
        if(KNOWN.containsKey(params[0])){
            return KNOWN.get(params[0]);
        }
        try {
            Bitmap res = BitmapFactory.decodeStream(params[0].openStream());
            KNOWN.put(params[0], res);
            return res;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        receiver.receive(bitmap);
    }
}
