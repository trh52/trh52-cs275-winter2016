package thighfill.l4;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author Tobias Highfill
 */
public class GeoLookup extends AsyncTask<Void, Void, JSONObject> {

    final MainActivity parent;

    public GeoLookup(MainActivity parent){
        this.parent = parent;
    }

    @Override
    protected JSONObject doInBackground(Void... params) {
        URL url = null;
        try {
            url = new URL(String.format(
                    "http://api.wunderground.com/api/%s/geolookup/q/autoip.json", parent.apiKey));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        JSONObject root = null;
        if (url != null) {
            try {
                root = new JSONObject(Util.readAll(url.openStream()));
                return root.getJSONObject("location");
            } catch (JSONException | IOException e) {
                e.printStackTrace();
                if(root != null){
                    try {
                        System.out.println(root.toString(2));
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(JSONObject loc) {
        if(loc != null){
            try {
                String zip = loc.getString("zip");
                new ForecastFetcher(parent).execute(zip);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
