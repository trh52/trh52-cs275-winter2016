package thighfill.l4;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * @author Tobias Highfill
 */
public class DatabaseManager {
    public static final String DB_NAME = "ForecastDB";
    public static final int DB_VERSION = 1;

    public static final String PRIMARY_KEY_DEF = "INTEGER PRIMARY KEY ASC ON CONFLICT FAIL AUTOINCREMENT";

    public static final String FORECAST_TABLE = "Forecasts";
    public static final String FETCHED_AT = "fetchedAt";
    public static final String JSON_DATA = "jsonData";
    public static final String CREATE_FORECAST = String.format(
            "CREATE TABLE IF NOT EXISTS %s (id %s, %s INTEGER, %s TEXT)",
            FORECAST_TABLE,
            PRIMARY_KEY_DEF,
            FETCHED_AT,
            JSON_DATA);

    OpenHelper helper;
    MainActivity parent;

    public DatabaseManager(Context context, MainActivity parent){
        helper = new OpenHelper(context);
        this.parent = parent;
    }

    public void saveForecast(JSONArray forecasts){
        long time = System.currentTimeMillis();
        ContentValues vals = new ContentValues();
        vals.put(FETCHED_AT, time);
        vals.put(JSON_DATA, forecasts.toString());
        SQLiteDatabase db = helper.getWritableDatabase();
        db.insert(FORECAST_TABLE, null, vals);
    }

    public JSONArray getRecentForecast(){
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cur = db.query(FORECAST_TABLE, null, null, null, null, null, FETCHED_AT + " DESC", "1");
        if(cur.getCount() < 1){
            cur.close();
            System.out.println("No entries found");
            return null;
        }
        cur.moveToFirst();
        long lastTime = cur.getLong(cur.getColumnIndex(FETCHED_AT));
        long currTime = System.currentTimeMillis();
        System.out.println("lastTime = "+lastTime+", currTime = "+currTime);
        if(currTime - lastTime < 1000*60*60){
            try {
                JSONArray res = new JSONArray(cur.getString(cur.getColumnIndex(JSON_DATA)));
                cur.close();
                return res;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        cur.close();
        return null;
    }

    private class OpenHelper extends SQLiteOpenHelper{
        private OpenHelper(Context context){
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_FORECAST);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }
}
