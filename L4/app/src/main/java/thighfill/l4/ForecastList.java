package thighfill.l4;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by mattress on 2/23/2016.
 */
public class ForecastList extends ArrayAdapter<Forecast> {

    public ForecastList(Context context){
        super(context, R.layout.list_item);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
//            convertView = super.getView(position, null, parent);
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item, parent, false);
        }
        LinearLayout root = (LinearLayout) convertView;
        Forecast forecast = this.getItem(position);
        for(int i = 0; i<root.getChildCount(); i++){
            View v = root.getChildAt(i);
            if(v instanceof ImageView){
                final ImageView icon = (ImageView) v;
                icon.setImageBitmap(forecast.icon);
                forecast.addIconUpdateListener(new Forecast.IconUpdateListener() {
                    @Override
                    public void iconUpdate(Bitmap newIcon) {
                        icon.setImageBitmap(newIcon);
                    }
                });
            }else if(v instanceof TextView){
                TextView txt = (TextView) v;
                txt.setText(String.format("[%s]\nCond: %s\tTemp: %d(F), %d(C)\tHumid: %d%%",
                        forecast.date, forecast.conditions, forecast.tempFahrenheit,
                        forecast.tempCelsius, forecast.humidity));
            }
        }
        return convertView;
    }
}
