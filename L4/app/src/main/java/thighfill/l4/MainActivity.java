package thighfill.l4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    ListView listView = null;
    ForecastList forecastList = null;

    String apiKey = null;

    DatabaseManager dataManager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        forecastList = new ForecastList(getApplicationContext());

        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(forecastList);

        apiKey = getString(R.string.wunderground_api);

        dataManager = new DatabaseManager(getApplicationContext(), this);

        new GeoLookup(this).execute();
    }

    public void usingCacheToast(){
        Toast.makeText(getApplicationContext(),
                R.string.using_cache_toast, Toast.LENGTH_SHORT).show();
    }
}
