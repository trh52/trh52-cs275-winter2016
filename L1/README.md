Lab 1 Web Service Clients
========
By Tobias Highfill (trh52)

This was pretty a straight-forward assignment. I have a static class Wunderground with a handful of static functions to do all the work. I created separate functions for reading input streams and for fetching and parsing the JSON from an API call. I used the JSON library from json.org because I felt it had a cleaner interface than gson.

The entry point is Wunderground.java just compile that.