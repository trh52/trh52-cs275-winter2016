import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;


public class LabFetcher {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			URL labURL = new URL("https://www.cs.drexel.edu/~augenbdh/cs275_wi16/labs/wxunderground.html");
			BufferedReader br = new BufferedReader(new InputStreamReader(labURL.openStream()));
			String line = br.readLine();
			while(line != null){
				System.out.println(line);
				line = br.readLine();
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
			System.exit(-1);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

}
