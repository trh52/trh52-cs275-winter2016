import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONObject;
import org.json.JSONArray;


public class Wunderground {
	public static final String API_KEY = "84ccd0c459fd937a";
	public static final String API_PREFIX = "http://api.wunderground.com/api/";
	
	public static String readAll(InputStream in) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String res = null;
		String line = br.readLine();
		while(line != null){
			if(res == null){
				res = line;
			}else{
				res += "\n" + line;
			}
			line = br.readLine();
		}
		return res;
	}
	
	public static JSONObject getJSON(String postfix){
		assert postfix.length() > 0;
		if(postfix.charAt(0)!='/'){
			postfix = '/'+postfix;
		}
		try {
			URL url = new URL(API_PREFIX + API_KEY + postfix);
			String jsonRaw = readAll(url.openStream());
			return new JSONObject(jsonRaw);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			System.exit(-1);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		return null; //Never reached... hopefully
	}
	
	public static JSONObject getLocation(){
		return getJSON("/geolookup/q/autoip.json").getJSONObject("location");
	}
	
	public static JSONArray getHourly(String zip){
		return getJSON("/hourly/q/"+zip+".json").getJSONArray("hourly_forecast");
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		JSONObject loc = getLocation();
		if(loc == null) System.exit(-1);
		String zip = loc.getString("zip");
		System.out.println("You are in " + loc.getString("city") + ", " + loc.getString("state"));
		System.out.println("Your zip code is " + zip);
		System.out.println("Your coordinates are: " + loc.getDouble("lat") + ", " + loc.getDouble("lon"));
		
		JSONArray hourly = getHourly(zip);
		if(hourly == null) System.exit(-1);
		for(int i=0; i<hourly.length(); i++){
			JSONObject curr = hourly.getJSONObject(i);
			String timestamp = curr.getJSONObject("FCTTIME").getString("pretty");
			int temp = curr.getJSONObject("temp").getInt("english");
			String cond = curr.getString("condition");
			int humidity = curr.getInt("humidity");
			System.out.println("["+timestamp + "]\tTemp:"+temp + "F\tHumidity: " + humidity + "%\tConditions: " + cond);
		}
	}

}
