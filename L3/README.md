CS 275 Lab 3 - Introduction to Android
=======
I made this in android studio so you will probably also need it to build/run this.

It's a very straightforward implementation. I created a GridLayout for the buttons
which are all labeled by their position. On activity creation I fetch all of them
into a neat 3x3 array.