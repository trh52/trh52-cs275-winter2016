package thighfill.l3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public static final int ROWS = 3, COLS = 3;
    public static final TicTacEntry FIRST_PLAYER = TicTacEntry.X;

    private Button[][] BUTTONS = null;
    public TicTacEntry[][] board = new TicTacEntry[ROWS][COLS];
    public TextView txtWinner = null;
    public TextView txtCurrent = null;
    public Button btnNewGame = null;
    public TicTacEntry currentPlayer = FIRST_PLAYER;
    private boolean gameOver = false;

    /**
     * Initialize the board buttons
     * @return A 3x3 array of Button objects corresponding to the board
     */
    private  Button[][] getButtons(){
        int[][] ids = {
                {R.id.btnTopLeft,R.id.btnTopCenter,R.id.btnTopRight},
                {R.id.btnMidLeft,R.id.btnMidCenter,R.id.btnMidRight},
                {R.id.btnBotLeft,R.id.btnBotCenter,R.id.btnBotRight}
        };
        Button[][] res = new Button[ROWS][COLS];
        for(int i=0; i<ROWS; i++){
            for(int j=0; j<COLS; j++){
                //Fetch the button
                res[i][j] = (Button) findViewById(ids[i][j]);
                //Save the location as final for the listener to access
                final int row=i, col=j;
                //Add the listener
                res[i][j].setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        //Check if the move is valid
                        //Game must be ongoing and the space must be empty
                        if(!gameOver && board[row][col] == null) {
                            //Get the button
                            Button b = (Button) v;
                            //Show the current player's mark on it
                            b.setText(currentPlayer.name());
                            //Update the game board
                            board[row][col] = currentPlayer;
                            //Switch the player
                            currentPlayer = currentPlayer.opposite();
                            //Update the GUI to match
                            updateCurrPlayer();
                            //Check to see if the game ends
                            checkWinner();
                        }else{
                            //Invalid move, bad player!
                            String msg = gameOver?"The game is over!":"That spot is taken!";
                            Toast toast = Toast.makeText(
                                    getApplicationContext(),
                                    msg,
                                    Toast.LENGTH_SHORT);
                            toast.show();
                        }
                    }
                });
            }
        }
        return res;
    }

    /**
     * Updates the GUI display telling the user whose turn it is
     */
    public void updateCurrPlayer(){
        String newPlayer = getString(R.string.to_play) + currentPlayer.name();
        txtCurrent.setText(newPlayer);
    }

    /**
     * Convenience function to check if three entries are equal and non-null
     * @param t1 The first entry
     * @param t2 The second entry
     * @param t3 The third entry
     * @return true if the entries are equal and not null
     */
    private boolean check3(TicTacEntry t1, TicTacEntry t2, TicTacEntry t3){
        return t1 != null && t1 == t2 && t1 == t3;
    }

    /**
     * Gets the game winner
     * @return The entry of the winning player or null if there is no winner
     */
    public TicTacEntry getWinner(){
        //Check rows and columns
        for(int i=0; i<3; i++) {
            if(check3(board[i][0], board[i][1], board[i][2])){
                return board[i][0];
            }
            if(check3(board[0][i], board[1][i], board[2][i])){
                return board[0][i];
            }
        }
        //Check diagonals
        if(check3(board[0][0], board[1][1], board[2][2]) ||
                check3(board[0][2], board[1][1], board[2][0])){
            return board[1][1]; //This is the one they have in common
        }
        return null;
    }

    /**
     * Checks if there are no null values on the board
     * @return true if there are no null values on the board, false otherwise.
     */
    public boolean isBoardFull(){
        for(TicTacEntry[] row : board){
            for(TicTacEntry t : row){
                if(t == null){
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Checks for a winner or a tie and ends the game if need be
     */
    public void checkWinner(){
        TicTacEntry winner = getWinner();
        if(winner == null && !isBoardFull()) {
            //No winner, game continues!
            return;
        }
        //Game Over!!
        gameOver = true;
        if(winner != null) {
            //There is a winner
            String announcement = getString(R.string.winner) + ' ' + winner.name();
            txtWinner.setText(announcement);
        }else{
            //The game is tied
            txtWinner.setText(R.string.its_tie);
        }
        //Show the results to the user
        txtWinner.setVisibility(View.VISIBLE);
        //Allow user to reset game
        btnNewGame.setVisibility(View.VISIBLE);
    }

    /**
     * Resets the board to it's initial state
     */
    public void resetGame(){
        txtWinner.setVisibility(View.INVISIBLE);
        btnNewGame.setVisibility(View.INVISIBLE);
        board = new TicTacEntry[ROWS][COLS];
        for(Button[] row : BUTTONS){
            for(Button b : row){
                b.setText("");
            }
        }
        currentPlayer = FIRST_PLAYER;
        updateCurrPlayer();
        gameOver = false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initialize variables
        BUTTONS = getButtons();
        txtWinner = (TextView) findViewById(R.id.txtWinner);
        txtCurrent = (TextView) findViewById(R.id.txtCurrent);
        btnNewGame = (Button) findViewById(R.id.btnNewGame);
        btnNewGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetGame();
            }
        });
        //Update the GUI
        updateCurrPlayer();
    }
}
