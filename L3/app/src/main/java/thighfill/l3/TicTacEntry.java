package thighfill.l3;

/**
 * Basic enum for TicTacToe data.
 * @author Tobias Highfill
 */
public enum TicTacEntry {
    X, O;

    /**
     * Gets the opposite of this value
     * @return X if this is O, X otherwise;
     */
    public TicTacEntry opposite(){
        switch(this){
            case X: return O;
            case O: return X;
            default: return null; //This should never happen
        }
    }
}
