Lab 2: OAuth
======
By: Tobias Highfill (trh52)

To build this you need to put your Google API credentials into a JSON file called
"client_secret.json" in the base "L2" directory. I included it in the .gitignore because 
I don't want someone causing trouble with my credentials.

You can download it from Google the same way I did at:

https://console.developers.google.com/apis/credentials

You need two sets of credentials:
 * One under "Other" to run the native google API
 * One under "Web Application" with the Temboo redirect URI for the Temboo version

Then just combine them into one file under "installed" and "web" respectively

You will also need to include the JSON library from json.org and Temboo's Java SDK in your build path.

I stored my Temboo credentials into another JSON file "TembooSecret.json" that is also in the .gitignore for the same reason.

It has the following top-level properties:
 * "user" the Temboo username
 * "app_name" the Temboo app's name
 * "key" the Temboo API key
 
All of that info can be found on Temboo's website.