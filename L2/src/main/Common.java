package main;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class Common {
	public static final String SCOPE = makeScope(
			"https://www.googleapis.com/auth/calendar",
			"https://www.googleapis.com/auth/calendar.readonly");
	
	/**
	 * This function reads everything in a Reader and concatenates it into a String
	 * @param r The reader
	 * @return The contents
	 * @throws IOException
	 */
	public static String readAll(Reader r) throws IOException{
		BufferedReader br = new BufferedReader(r);
		StringBuilder res = new StringBuilder();
		String line = br.readLine();
		while(line != null){
			if(res.length() == 0){
				res.append(line);
			}else{
				res.append("\n"+line);
			}
			line = br.readLine();
		}
		return res.toString();
	}
	
	private static String makeScope(String... scopes) {
		StringBuilder res = new StringBuilder();
		for(int i=0; i<scopes.length; i++){
			if(i != 0){
				res.append("%20");
			}
			res.append(scopes[i]);
		}
		return res.toString();
	}

	/**
	 * This function reads everything in an InputStream and concatenates it into a String
	 * @param in The InputStream
	 * @return The contents or null if there was a problem
	 */
	public static String readAll(InputStream in){
		try {
			return readAll(new InputStreamReader(in));
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		return null;
	}

	/**
	 * This function reads everything in a File and concatenates it into a String
	 * @param f The File
	 * @return The contents or null if there was a problem
	 */
	public static String readAll(File f){
		try {
			return readAll(new FileReader(f));
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
		return null;
	}
	
	public static JSONObject printAllCalendars(JSONArray cals){
		JSONObject first = null;
		System.out.println("Calendars found:");
		for(int i=0; i<cals.length(); i++){
			JSONObject curr = cals.getJSONObject(i);
			if(first == null) {
				first = curr;
			}
			System.out.println(curr.getString("summary"));
		}
		return first;
	}
	
	public static void printAllEvents(JSONArray events, String name){
		System.out.println("\nEvents in calendar \"" + name + "\":");
		for(int i=0; i<events.length(); i++){
			JSONObject event = events.getJSONObject(i);
			JSONObject start = event.getJSONObject("start");
			String dateTime = null;
			String timestamp = null;
			try {
				dateTime = start.getString("dateTime");
				String date = dateTime.substring(0, 10);
				String time = dateTime.substring(11, 19);
				String timezone = dateTime.substring(19);
				timestamp = String.format("[%s %s (UTC%s)]", date, time, timezone);
			} catch (JSONException e) {
				timestamp = '[' + start.getString("date") + ']';
			}
			System.out.println(String.format("%s %s", timestamp, event.getString("summary")));
		}
	}
}
