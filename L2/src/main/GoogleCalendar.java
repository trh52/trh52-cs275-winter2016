package main;
import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;

//import static main.Common.*;

public class GoogleCalendar {
	public static final JSONObject SECRETS =
			new JSONObject(Common.readAll(new File("client_secret.json"))).getJSONObject("installed");
	public static final String CLIENT_ID = SECRETS.getString("client_id");
	public static final String CLIENT_SECRET = SECRETS.getString("client_secret");

	private static final int LISTENER_PORT = 9009;
	private static ServerSocket LISTENER;
	private static final StringBuffer LISTEN_BUFFER = new StringBuffer();

	private static String USER_CODE;
	private static String ACCESS_TOKEN;

	/**
	 * This function initializes the server socket and starts the listen thread
	 */
	private static void startListening(){
		try {
			LISTENER = new ServerSocket(LISTENER_PORT);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		Thread listenThread = new Thread(new Runnable(){
			@Override
			public void run() {
				try {
					Socket s = LISTENER.accept();
//					System.out.println("Got connection!");
					BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
					String resp = br.readLine();
//					System.out.println("response:\n"+resp);
					LISTEN_BUFFER.append(resp);
					s.close();
				} catch (IOException e) {
					e.printStackTrace();
					System.exit(-1);
				}
			}
		});
		listenThread.start();
	}
	
	

	/**
	 * Sends a "POST" HTTP request and returns the contents of the response
	 * 
	 * I found this snippet on stack overflow
	 * 
	 * http://stackoverflow.com/questions/1359689/how-to-send-http-request-in-java
	 * 
	 * @param targetURL The URL to target
	 * @param urlParameters The parameters to pass
	 * @return The contents of the response
	 */
	public static String executePost(String targetURL, String urlParameters) {
		HttpURLConnection connection = null;  
		try {
			//Create connection
			URL url = new URL(targetURL);
			connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", 
					"application/x-www-form-urlencoded");

			connection.setRequestProperty("Content-Length", 
					Integer.toString(urlParameters.getBytes().length));
			connection.setRequestProperty("Content-Language", "en-US");  

			connection.setUseCaches(false);
			connection.setDoOutput(true);

			//Send request
			DataOutputStream wr = new DataOutputStream (
					connection.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.close();

			//Get Response  
			return Common.readAll(connection.getInputStream());
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
			return null;
		} finally {
			if(connection != null) {
				connection.disconnect(); 
			}
		}
	}

	/**
	 * Does all of the OAuth stuff
	 */
	public static void authorize(){
		String redirect = "http://localhost:"+LISTENER_PORT;
		try {
			URL authURL = new URL(SECRETS.getString("auth_uri")
					+"?client_id="+CLIENT_ID
					+"&scope="+Common.SCOPE+"&response_type=code"
					+"&redirect_uri="+redirect);
			Desktop.getDesktop().browse(authURL.toURI());
			while(LISTEN_BUFFER.length()==0){}//Wait for the listener thread
			USER_CODE = LISTEN_BUFFER.toString().substring(11, 56);
//			System.out.println("code="+USER_CODE);

			String resp = executePost(SECRETS.getString("token_uri"),
					"client_id="+CLIENT_ID
					+"&code="+USER_CODE
					+"&client_secret="+CLIENT_SECRET
					+"&redirect_uri="+redirect
					+"&grant_type=authorization_code");
//			System.out.println("resp:\n"+resp);
			ACCESS_TOKEN = new JSONObject(resp).getString("access_token");
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	public static String executeGet(String urlstr){
		try {
			return Common.readAll(new URL(urlstr).openStream());
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
		return null;
	}
	
	public static JSONArray getCalendars(){
		return new JSONObject(executeGet(
				"https://www.googleapis.com/calendar/v3/users/me/calendarList?access_token="+ACCESS_TOKEN))
				.getJSONArray("items");
	}
	
	public static JSONArray getEvents(String calID){
		return new JSONObject(executeGet(
				"https://www.googleapis.com/calendar/v3/calendars/"+calID+"/events?access_token="+ACCESS_TOKEN)
				).getJSONArray("items");
	}

	public static void main(String[] args) {
		startListening();
		authorize();
		
		JSONArray cals = getCalendars();
		JSONObject firstCal = Common.printAllCalendars(cals);
		
		JSONArray events = getEvents(firstCal.getString("id"));
		Common.printAllEvents(events, firstCal.getString("summary"));
		System.exit(0);
	}

}
