package main;
import java.awt.Desktop;
import java.io.File;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;

import com.temboo.Library.Google.Calendar.GetAllCalendars;
import com.temboo.Library.Google.Calendar.GetAllCalendars.GetAllCalendarsInputSet;
import com.temboo.Library.Google.Calendar.GetAllCalendars.GetAllCalendarsResultSet;
import com.temboo.Library.Google.Calendar.GetAllEvents;
import com.temboo.Library.Google.Calendar.GetAllEvents.GetAllEventsInputSet;
import com.temboo.Library.Google.Calendar.GetAllEvents.GetAllEventsResultSet;
import com.temboo.Library.Google.OAuth.*;
import com.temboo.Library.Google.OAuth.InitializeOAuth.*;
import com.temboo.Library.Google.OAuth.FinalizeOAuth.*;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;


public class TembooGoogleCalendar {
	public static final JSONObject SECRETS =
			new JSONObject(Common.readAll(new File("client_secret.json"))).getJSONObject("web");
	public static final String CLIENT_ID = SECRETS.getString("client_id");
	public static final String CLIENT_SECRET = SECRETS.getString("client_secret");
	
	private static final String REDIRECT = "https://trh52.temboolive.com/callback/google"; 
	private static final TembooSession SESSION = initTemboo();
	private static String ACCESS_TOKEN = null;
	private static String REFRESH_TOKEN = null;
	
	private static TembooSession initTemboo(){
		JSONObject tembooInfo = new JSONObject(Common.readAll(new File("TembooSecret.json")));
		try {
			return new TembooSession(
					tembooInfo.getString("user"),
					tembooInfo.getString("app_name"),
					tembooInfo.getString("key"));
		} catch (TembooException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		return null;
	}
	
	private static void authorize(){
		try {
			InitializeOAuth initChoreo = new InitializeOAuth(SESSION);
			InitializeOAuthInputSet initInputs = initChoreo.newInputSet();
			
			initInputs.set_ClientID(CLIENT_ID);
			initInputs.set_Scope(Common.SCOPE.replaceAll("%20", " "));
			initInputs.set_ForwardingURL(REDIRECT);
			
			InitializeOAuthResultSet initResults = initChoreo.execute(initInputs);
			String callbackID = initResults.get_CallbackID();
			URL authURL = new URL(initResults.get_AuthorizationURL());
			Desktop.getDesktop().browse(authURL.toURI());
			
			FinalizeOAuth finalChoreo = new FinalizeOAuth(SESSION);
			FinalizeOAuthInputSet finalInputs = finalChoreo.newInputSet();
			
			finalInputs.set_ClientID(CLIENT_ID);
			finalInputs.set_ClientSecret(CLIENT_SECRET);
			finalInputs.set_CallbackID(callbackID);
			
			FinalizeOAuthResultSet finalResults = finalChoreo.execute(finalInputs);
			ACCESS_TOKEN = finalResults.get_AccessToken();
			REFRESH_TOKEN = finalResults.get_RefreshToken();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	public static JSONArray getCalendars(boolean refresh){
		GetAllCalendars choreo = new GetAllCalendars(SESSION);
		GetAllCalendarsInputSet inputs = choreo.newInputSet();
		if(refresh){
			inputs.set_RefreshToken(REFRESH_TOKEN);
		}else{
			inputs.set_AccessToken(ACCESS_TOKEN);
		}
		inputs.set_ClientID(CLIENT_ID);
		inputs.set_ClientSecret(CLIENT_SECRET);
		
		try {
			GetAllCalendarsResultSet results = choreo.execute(inputs);
			if(refresh){
				ACCESS_TOKEN = results.get_NewAccessToken();
			}
			return new JSONObject(results.get_Response()).getJSONArray("items");
		} catch (TembooException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		return null;
	}
	
	public static JSONArray getEvents(boolean refresh, String calID){
		GetAllEvents choreo = new GetAllEvents(SESSION);
		GetAllEventsInputSet inputs = choreo.newInputSet();
		if(refresh){
			inputs.set_RefreshToken(REFRESH_TOKEN);
		}else{
			inputs.set_AccessToken(ACCESS_TOKEN);
		}
		inputs.set_ClientID(CLIENT_ID);
		inputs.set_ClientSecret(CLIENT_SECRET);
		inputs.set_CalendarID(calID);
		
		try {
			GetAllEventsResultSet results = choreo.execute(inputs);
			if(refresh){
				ACCESS_TOKEN = results.get_NewAccessToken();
			}
			return new JSONObject(results.get_Response()).getJSONArray("items");
		} catch (TembooException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		return null;
	}

	public static void main(String[] args) {
		authorize();
		JSONArray cals = getCalendars(false);
		JSONObject firstCal = Common.printAllCalendars(cals);
		JSONArray events = getEvents(false, firstCal.getString("id"));
		Common.printAllEvents(events, firstCal.getString("summary"));
	}

}
