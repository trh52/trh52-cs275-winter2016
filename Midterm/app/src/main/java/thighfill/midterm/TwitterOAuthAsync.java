package thighfill.midterm;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.View;

import com.temboo.Library.Twitter.OAuth.FinalizeOAuth;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth.*;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth.*;
import com.temboo.core.TembooException;

/**
 * @author Tobias Highfill
 */
public class TwitterOAuthAsync  extends AsyncTask<String,Void,String[]>{

    final MainActivity parent;
    public boolean failed = false;

    private static final String pref_AT = "twitterAccessToken";
    private static final String pref_ATS = "twitterAccessTokenSecret";

    SharedPreferences prefs = null;

    public TwitterOAuthAsync(MainActivity parent){
        this.parent = parent;
        prefs = parent.getSharedPreferences(MainActivity.PREFS_NAME, 0);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        this.parent.setStatus(R.string.status_Authorizing);
        this.parent.setBarIndeterminate(true);
        this.parent.barProgress.setVisibility(View.VISIBLE);
        if(parent.accessToken == null) {
            parent.accessToken = prefs.getString(pref_AT, null);
        }
        if(parent.accessTokenSecret == null) {
            parent.accessTokenSecret = prefs.getString(pref_ATS, null);
        }
    }

    @Override
    protected String[] doInBackground(String... params) {
        String APIkey = params[0];
        String APIsecret = params[1];

        if(parent.accessToken != null && parent.accessTokenSecret != null){
            return new String[]{parent.accessToken, parent.accessTokenSecret};
        }


        InitializeOAuth initChoreo = new InitializeOAuth(parent.session);
        InitializeOAuthInputSet initInputs = initChoreo.newInputSet();

        initInputs.set_ConsumerKey(APIkey);
        initInputs.set_ConsumerSecret(APIsecret);

        InitializeOAuthResultSet initResults;
        try {
            initResults = initChoreo.execute(initInputs);
        } catch (TembooException e) {
            Util.handleException(e, parent.getApplicationContext());
            return null;
        }
        String token = initResults.get_OAuthTokenSecret();

        Intent authIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(initResults.get_AuthorizationURL()));
        parent.startActivity(authIntent);

        FinalizeOAuth finalChoreo = new FinalizeOAuth(parent.session);
        FinalizeOAuthInputSet finalInputs = finalChoreo.newInputSet();

        finalInputs.set_ConsumerKey(APIkey);
        finalInputs.set_ConsumerSecret(APIsecret);
        finalInputs.set_OAuthTokenSecret(token);
        finalInputs.set_CallbackID(initResults.get_CallbackID());

        try {
            FinalizeOAuthResultSet finalResults = finalChoreo.execute(finalInputs);
            return new String[]{
                    finalResults.get_AccessToken(),
                    finalResults.get_AccessTokenSecret()};
        } catch (TembooException e) {
            Util.handleException(e, parent.getApplicationContext());
        }

        return null;
    }

    @Override
    protected void onPostExecute(String[] resultSet) {
        failed = resultSet == null;
        if(!failed) {
            parent.accessToken = resultSet[0];
            parent.accessTokenSecret = resultSet[1];
            parent.btnGo.setEnabled(true);

            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(pref_AT, parent.accessToken);
            editor.putString(pref_ATS, parent.accessTokenSecret);
            editor.apply();
        }
        parent.lblStatus.setText(R.string.status_Ready);
        parent.barProgress.setVisibility(View.INVISIBLE);
    }
}
