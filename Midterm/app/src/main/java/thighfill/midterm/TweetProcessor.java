package thighfill.midterm;

import android.os.AsyncTask;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * @author Tobias Highfill
 */
public class TweetProcessor extends AsyncTask<JSONArray, Integer, Double>{

    private final MainActivity parent;
    private List<String> polysToAdd = new LinkedList<>();

    public TweetProcessor(MainActivity parent){
        this.parent = parent;
    }

    public void flushPolys(){
        while(!polysToAdd.isEmpty()){
            parent.lblShowPolys.append(polysToAdd.remove(0) + '\n');
        }
    }

    @Override
    protected void onPreExecute() {
        parent.setStatus(R.string.status_ProcTweets);
        parent.barProgress.setVisibility(View.VISIBLE);
        parent.barProgress.setIndeterminate(false);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        parent.barProgress.setProgress(values[0]);
        if(values.length > 1) {
            parent.barProgress.setMax(values[1]);
        }
        parent.barProgress.setIndeterminate(false);
        flushPolys();
    }

    @Override
    protected Double doInBackground(JSONArray... params) {
        JSONArray tweets = params[0];
        final int length = tweets.length();
        this.publishProgress(0, length);
        int polyCount = 0;
        int senCount = 0;
        for(int i=0; i<length; i++){
            try {
                String text = tweets.getJSONObject(i).getString("text");
                String delimiters = "/\\#.!?, \t\n\r\f";
                StringTokenizer tokenizer = new StringTokenizer(text, delimiters, true);
                boolean wasWord = false;
                while(tokenizer.hasMoreTokens()){
                    String token = tokenizer.nextToken();
                    switch(token.charAt(0)){//Switch on first char to catch delimiters I care about
                        case '.':case '!':case '?':
                            if(wasWord) {
                                senCount++;
                            }
                            wasWord = false;
                            break;
                        default: //This token is a word
                            if(delimiters.contains(token)){
                                break; //ignore the delimiters
                            }
                            int syllables =parent.syllCount.countSyllables(token);
                            if(syllables > 2) {
                                polyCount++;
                                polysToAdd.add(String.format("%s (%d)", token, syllables));
                            }
                            wasWord = true;
                            break;
                    }
                }
                if(wasWord){
                    senCount++; //End of the tweet counts as a sentence
                }
            } catch (JSONException e) {
                Util.handleException(e, parent.getApplicationContext());
            }
            this.publishProgress(i+1);
        }
        return 1.0430 * Math.sqrt(polyCount * (30.0 / senCount)) + 3.1291;
    }

    @Override
    protected void onPostExecute(Double grade) {
        flushPolys();
        parent.lblOutput.setText(String.format("Grade: %f", grade));
        parent.lblOutput.setVisibility(View.VISIBLE);
        parent.lblStatus.setText(R.string.status_Ready);
        parent.barProgress.setVisibility(View.INVISIBLE);
        parent.btnGo.setEnabled(true); //Re-enable the go button now that we're done
    }
}
