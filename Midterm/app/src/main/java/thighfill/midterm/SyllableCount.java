package thighfill.midterm;

import org.json.JSONArray;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Tobias Highfill
 */
public class SyllableCount {

    private static final Map<String, Integer> KNOWN = new HashMap<>();
    private final String APIkey;
    private MainActivity parent;
    public boolean useCanonical = false;
    public int limit = 50;

    public SyllableCount(String APIkey, MainActivity parent){
        this.APIkey = APIkey;
        this.parent = parent;
    }

    public int countSyllables(String word){
        word = word.trim().toLowerCase();
        if(KNOWN.containsKey(word)){
            int val = KNOWN.get(word);
            if(val > 0){
                return val;
            }
        }
        int res = this.lookupWord(word);
        if(res > 0) {
            KNOWN.put(word, res);
        }
        return res;
    }

    private int lookupWord(String word){
        if(word.length() <= 3){
            return 1;
        }
        try {
            URL url = new URL(String.format(
                    "http://api.wordnik.com:80/v4/word.json/%s/hyphenation?useCanonical=%s&limit=%d&api_key=%s",
                    word,
                    ""+useCanonical,
                    limit,
                    APIkey));
            JSONArray arr = new JSONArray(Util.readAll(url.openStream()));
            return arr.length();
        } catch (Exception e) {
            Util.handleException(e, parent.getApplicationContext());
            System.err.println("Error on the lookup for \"" + word + '"');
        }
        return 1; //word probably doesn't exist assume one syllable
    }
}
