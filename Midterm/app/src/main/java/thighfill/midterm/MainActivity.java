package thighfill.midterm;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;

import org.json.JSONArray;

public class MainActivity extends AppCompatActivity {

    public static final String PREFS_NAME = "MidtermPrefs";

    public TembooSession session;

    public EditText txtHandle = null;
    public Button btnGo = null;
    public TextView lblOutput = null;
    public TextView lblStatus = null;
    public TextView lblShowPolys = null;
    public ProgressBar barProgress = null;

    public String consumerKey = null;
    public String consumerSecret = null;
    public String accessToken = null;
    public String accessTokenSecret = null;

    public SyllableCount syllCount = null;


    private TembooSession initTemboo(){
        try {
            return new TembooSession(
                    getString(R.string.temboo_username),
                    getString(R.string.temboo_appname),
                    getString(R.string.temboo_key));
        } catch (TembooException e) {
            Util.handleException(e, getApplicationContext());
            return null;
        }
    }

    public void lookupHandle(String handle){
        new TweetsAsync(this).execute(handle);
    }

    public void processTweets(JSONArray tweets){
        new TweetProcessor(this).execute(tweets);
    }

    public void setStatus(String status){
        lblStatus.setText(status);
    }

    public void setStatus(int id){
        setStatus(getString(id));
    }

    public void setBarIndeterminate(boolean indeterminate){
        barProgress.setIndeterminate(indeterminate);
    }

    public void startEverything(){
        lblShowPolys.setText("");
        lblOutput.setVisibility(View.INVISIBLE);
        lookupHandle(txtHandle.getText().toString());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        session = initTemboo();
        syllCount = new SyllableCount(getString(R.string.wordnik_APIkey), this);

        consumerKey = getString(R.string.twitter_APIkey);
        consumerSecret = getString(R.string.twitter_APIsecret);

        txtHandle = (EditText) findViewById(R.id.txtHandle);
        btnGo = (Button) findViewById(R.id.btnGo);
        lblOutput = (TextView) findViewById(R.id.lblOutput);
        lblStatus = (TextView) findViewById(R.id.lblStatus);
        lblShowPolys = (TextView) findViewById(R.id.lblShowPolys);
        barProgress = (ProgressBar) findViewById(R.id.barProgress);

        txtHandle.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(btnGo.isEnabled() && (keyCode == KeyEvent.KEYCODE_ENTER
                        || keyCode == KeyEvent.KEYCODE_NUMPAD_ENTER
                        || keyCode == KeyEvent.KEYCODE_DPAD_CENTER)){
                    startEverything();
                    return true;
                }
                return false;
            }
        });

        btnGo.setEnabled(false);
        btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startEverything();
            }
        });

//        barProgress.setRight(barProgress.getRight()-btnGo.getWidth());

        accessToken = getString(R.string.twitter_PersonalAT);
        accessTokenSecret = getString(R.string.twitter_PersonalATS);
        new TwitterOAuthAsync(this).execute(consumerKey, consumerSecret);
    }
}
