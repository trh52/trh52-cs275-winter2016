package thighfill.midterm;

import android.content.Context;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by mattress on 2/10/2016.
 */
public class Util {
    public static void handleException(Exception e, Context context){
        e.printStackTrace();
//        Toast toast = Toast.makeText(context, e.getClass().getCanonicalName(), Toast.LENGTH_LONG);
//        toast.show();
    }
    public static String readAll(InputStream in){
        StringBuilder res = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String line = null;
        try {
            line = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        while(line != null){
            res.append(line);
            res.append('\n');
            try {
                line = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
                line = null;
            }
        }
        return res.toString();
    }
}
