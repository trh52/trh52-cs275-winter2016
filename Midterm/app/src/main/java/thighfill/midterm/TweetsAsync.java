package thighfill.midterm;

import android.os.AsyncTask;
import android.view.View;

import com.temboo.Library.Twitter.Search.Tweets;
import com.temboo.Library.Twitter.Search.Tweets.TweetsInputSet;
import com.temboo.Library.Twitter.Search.Tweets.TweetsResultSet;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Tobias Highfill
 */
public class TweetsAsync extends AsyncTask<String, Void, JSONObject> {

    private final MainActivity parent;
    public int fetchCount = 30;

    public TweetsAsync(MainActivity mainActivity) {
        parent = mainActivity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        //Update status
        parent.lblStatus.setText(R.string.status_FetchingTweets);
        //Update the progress bar
        parent.barProgress.setVisibility(View.VISIBLE);
        parent.setBarIndeterminate(true);
        //Disable the go button so the user can't send another request
        parent.btnGo.setEnabled(false);
    }

    @Override
    protected JSONObject doInBackground(String... params) {
        String handle = params[0].trim(); //Trimmed the whitespace
        Tweets choreo = new Tweets(parent.session);
        TweetsInputSet inputs = choreo.newInputSet();

        inputs.set_ConsumerKey(parent.consumerKey);
        inputs.set_ConsumerSecret(parent.consumerSecret);
        inputs.set_AccessToken(parent.accessToken);
        inputs.set_AccessTokenSecret(parent.accessTokenSecret);
        inputs.set_Query("from:" + handle);
        inputs.set_Count(fetchCount);

        try {
            TweetsResultSet results = choreo.execute(inputs);
            return new JSONObject(results.get_Response());
        } catch (Exception e) {
            Util.handleException(e, parent.getApplicationContext());
        }
        return null;
    }

    @Override
    protected void onPostExecute(JSONObject root) {
        parent.lblStatus.setText(R.string.status_Ready);
        try {
            parent.processTweets(root.getJSONArray("statuses"));
        } catch (JSONException e) {
            Util.handleException(e, parent.getApplicationContext());
        }
    }
}
