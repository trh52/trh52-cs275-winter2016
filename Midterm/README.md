CS 275 Midterm Practicum - Twitter Grader
=====

By: Tobias Highfill (trh52)

I decided to do this in Android just for fun so
you will need Android Studio to run this program.
You will also need Temboo's Android SDK for Twitter
and the json.org library in your classpath.

I parse the words using Java's StringTokenizer class to break
a tweet up into tokens that I can process. If the token is
a sentence delimiter (period, exclamation point, or question mark)
I check to see if the sentence wasn't empty (had some valid tokens)
and increment the sentence counter. If it was some other delimiter
(such as whitespace or slashes or a pound sign) I ignore it.
Otherwise I treat the token as a word and count it's syllables.

On a side note, I had to use a really old outdated style for the
progress bar. Why isn't there some way to set a newer style but say
"fall back to this old one if it's unavailable"?