package thighfill.a2;

import android.content.Context;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Tobias Highfill
 */
public class FriendEmailAdapter extends ArrayAdapter<Object>{

    public static boolean isEmailValid(CharSequence email){
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private Map<Integer, EditText> editors = new HashMap<>();

    public FriendEmailAdapter(Context context){
        super(context, R.layout.friend_list_item);
        add(new Object());
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.friend_list_item, parent, false);
            final EditText txtFriendEmail;
            if(editors.containsKey(position)){
                txtFriendEmail = editors.get(position);
            }else{
                txtFriendEmail = (EditText) convertView.findViewById(R.id.txtFriendEmail);
            }
            txtFriendEmail.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if(keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN){
                        insert(txtFriendEmail.getText().toString(), position);
                    }
                    return false;
                }
            });
            editors.put(position, txtFriendEmail);
        }
        return convertView;
    }

    public List<String> getEmails(){
        int count = this.getCount();
        ArrayList<String> tmp = new ArrayList<>(count);
        for(int i=0; i < count; i++){
            String email = editors.get(i).getText().toString().trim();
            if(isEmailValid(email)){
                tmp.add(email);
                System.out.println("Found email: \""+email+'"');
            }else{
                System.out.println("Found not email: \""+email+'"');
            }
        }
        return tmp;
    }
}
