package thighfill.a2;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import junit.framework.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 */
public class FriendLocationAdapter extends ArrayAdapter<FriendLocation> {

    private MainActivity parent;

    public FriendLocationAdapter(MainActivity parent) {
        super(parent.getApplicationContext(), R.layout.friend_loc);
        this.parent = parent;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.friend_loc, parent, false);
            convertView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    //TODO: put in messaging thing
                    return false;
                }
            });
        }
        final FriendLocation loc = this.getItem(position);
        TextView lblFriendName = (TextView) convertView.findViewById(R.id.lblFriendName);
        TextView lblFriendLoc = (TextView) convertView.findViewById(R.id.lblFriendLoc);
        TextView lblFriendTime = (TextView) convertView.findViewById(R.id.lblFriendTime);

        Date date = new Date(loc.time);

        lblFriendName.setText(loc.username);
        lblFriendLoc.setText(String.format("Lat: %f, Lon: %f", loc.latitude, loc.longitiude));
        lblFriendTime.setText(date.toString());

        convertView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                sendEmail(loc);
                return true;
            }
        });

        return convertView;
    }

    public List<FriendLocation> getAll(){
        List<FriendLocation> res = new ArrayList<>(this.getCount());
        for(int i=0; i<this.getCount(); i++){
            res.add(this.getItem(i));
        }
        return res;
    }

    private void sendEmail(FriendLocation loc){
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{loc.username});
        i.putExtra(Intent.EXTRA_SUBJECT, "Saw you were in the area");
        i.putExtra(Intent.EXTRA_TEXT   , "");
        try {
            parent.startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getContext(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }
}
