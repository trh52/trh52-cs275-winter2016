package thighfill.a2;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.temboo.Library.CloudMine.ObjectStorage.ObjectGet;
import com.temboo.Library.CloudMine.ObjectStorage.ObjectSet;
import com.temboo.Library.CloudMine.UserAccountManagement.AccountLogout;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_LOCATION = 42;

    public static final String PREFS_NAME = "A2prefs_trh52";
    public static final String PREF_CL_TOKEN = "CLtoken";
    public static final String PREF_CL_NAME = "CLname";
    public static final String PREF_CL_PASS = "CLpass";
    public static final String PREF_CL_REM_PASS = "CLremPass";

    public static final String PREF_RADIUS = "radius";
    public static final String PREF_WINDOW = "window";

    public static final String EXTRA_LATITUDES = "Latitudes";
    public static final String EXTRA_LONGITUDES = "Longitudes";
    public static final String EXTRA_NAMES = "Names";
    public static final String EXTRA_TIMES = "Times";

    public static final String FRIENDS_SUFFIX = "_friends";
    public static final String LOCATION_SUFFIX = "_location";

    public static final TembooSession TEMBOO_SESSION = initTemboo();

    private static String cloudmineAPI = null;
    private static String cloudmineAppID = null;

    private static TembooSession initTemboo() {
        TembooSession res = null;
        try {
            res = new TembooSession("trh52", "myFirstApp", "yOi29FTaHDLQBaK10RBx6yHo9ftTBgbq");
        } catch (TembooException e) {
            e.printStackTrace();
        }
        assert res != null;
        return res;
    }

    public static String getCloudmineAPI() {
        assert cloudmineAPI != null;
        return cloudmineAPI;
    }

    public static String getCloudmineAppID() {
        assert cloudmineAppID != null;
        return cloudmineAppID;
    }

    SharedPreferences prefs;

    private LocationManager locationManager = null;
    private long minTime = 1000 * 60 * 60; //One hour
    private float minDistance = 100; //Meters
    private Location lastLocation = null;
    private long timeUpdated = -1;

    ProgressBar barLoading = null;
    TextView lblCurLocation = null;
    EditText numRadius = null;
    EditText numWindow = null;
    Button btnAddFriend = null;
    Button btnWhosAround = null;
    Button btnLogInOut = null;
    ListView lstOutput = null;
    Button btnMapIt = null;

    boolean loggedIn = false;

    List<String> friends = new LinkedList<>();

    FriendLocationAdapter adapter = null;

    private PopupWindow popupWindow = null;

    private GetFriendsTask getFriendsTask = null;
    private StoreLocationTask storeLocationTask = null;

    private String adminToken = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        prefs = getSharedPreferences(PREFS_NAME, 0);

        cloudmineAPI = getString(R.string.cloudmine_key);
        cloudmineAppID = getString(R.string.cloudmine_app_id);

        lstOutput = (ListView) findViewById(R.id.lstOutput);
        adapter = new FriendLocationAdapter(this);
        lstOutput.setAdapter(adapter);

        barLoading = (ProgressBar) findViewById(R.id.barLoading);
        lblCurLocation = (TextView) findViewById(R.id.lblCurLocation);
        numRadius = (EditText) findViewById(R.id.numRadius);
        numWindow = (EditText) findViewById(R.id.numWindow);
        btnAddFriend = (Button) findViewById(R.id.btnAddFriend);
        btnWhosAround = (Button) findViewById(R.id.btnWhosAround);
        btnLogInOut = (Button) findViewById(R.id.btnLogInOut);
        btnMapIt = (Button) findViewById(R.id.btnMapIt);

        updateLocationDisplay();
        adminLogIn();

        btnAddFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFriend();
            }
        });
        btnWhosAround.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshFriends();
            }
        });
        btnMapIt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMapsActivity();
            }
        });
        btnLogInOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (loggedIn) {
                    logOut();
                } else {
                    logIn();
                }
            }
        });

        numRadius.setText(prefs.getString(PREF_RADIUS, numRadius.getText().toString()));
        numWindow.setText(prefs.getString(PREF_WINDOW, numWindow.getText().toString()));

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        setUpLocationManager();

        SharedPreferences.OnSharedPreferenceChangeListener prefListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                if (key.equals(PREF_CL_TOKEN)) {
                    String newVal = sharedPreferences.getString(key, null);
                    loggedIn = !(newVal == null || newVal.isEmpty());
                    if (loggedIn) {
                        btnLogInOut.setText(R.string.action_log_out);
                    } else {
                        btnLogInOut.setText(R.string.action_log_in);
                    }
                    btnWhosAround.setEnabled(loggedIn);
                    btnAddFriend.setEnabled(loggedIn);
                    btnMapIt.setEnabled(loggedIn);
                }
            }
        };

        prefs.registerOnSharedPreferenceChangeListener(prefListener);
        //Create the event to set everything up
        prefListener.onSharedPreferenceChanged(prefs, PREF_CL_TOKEN);

        String token = getUserToken();
        if (token == null) {
            logIn();
        }
    }

    private void startMapsActivity(){
        Intent intent = new Intent(this, MapsActivity.class);
        List<FriendLocation> friendLocations = adapter.getAll();

        double[] longitudes = new double[friendLocations.size()+1];
        double[] latitudes = new double[longitudes.length];
        String[] emails = new String[longitudes.length];
        long[] times = new long[longitudes.length];

        longitudes[0] = lastLocation.getLongitude();
        latitudes[0] = lastLocation.getLatitude();
        emails[0] = "Me";
        times[0] = timeUpdated;

        for(int i=1; i<longitudes.length; i++){
            FriendLocation loc = friendLocations.get(i-1);
            longitudes[i] = loc.longitiude;
            latitudes[i] = loc.latitude;
            emails[i] = loc.username;
            times[i] = loc.time;
        }

        intent.putExtra(EXTRA_LATITUDES, latitudes);
        intent.putExtra(EXTRA_LONGITUDES, longitudes);
        intent.putExtra(EXTRA_NAMES, emails);
        intent.putExtra(EXTRA_TIMES, times);
        startActivity(intent);
    }

    private void adminLogIn(){
        new AdminLoginTask().execute();
    }

    private void logOut(){
        final MainActivity me = this;
        new Thread(new Runnable() {
            @Override
            public void run() {
                AccountLogout logout = new AccountLogout(TEMBOO_SESSION);
                AccountLogout.AccountLogoutInputSet inputSet = logout.newInputSet();

                inputSet.set_APIKey(cloudmineAPI);
                inputSet.set_ApplicationIdentifier(cloudmineAppID);
                inputSet.set_SessionToken(getUserToken());

                try {
                    logout.executeNoResults(inputSet);
                    for(AsyncTask<?,?,?> task : new AsyncTask<?,?,?>[]{getFriendsTask, storeLocationTask}) {
                        if (task != null && task.getStatus() == AsyncTask.Status.RUNNING) {
                            task.cancel(true);
                        }
                    }
//                    prefs.edit().putString(PREF_CL_TOKEN, null).apply();
                    friends = null;
                    adapter = new FriendLocationAdapter(me);
//                    lstOutput.setAdapter(adapter);
                    logIn();
                } catch (TembooException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void logIn(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    private String getUserToken(){
        return prefs.getString(PREF_CL_TOKEN, null);
    }

    private String getUsername(){
        return prefs.getString(PREF_CL_NAME, null);
    }

    private String scrubEmail(String email){
        return email.replace('@','_').replace('.','_').toLowerCase();
    }

    private String getLocationKey(){
        return getLocationKey(getUsername());
    }

    private String getLocationKey(String email){
        return scrubEmail(email) + LOCATION_SUFFIX;
    }

    private String getFriendsKey(){
        return getFriendsKey(getUsername());
    }

    private String getFriendsKey(String email){
        return scrubEmail(email) + FRIENDS_SUFFIX;
    }

    private double getRadius(){
        return Double.parseDouble(numRadius.getText().toString());
    }

    private long getTimingWindow(){
        return Long.parseLong(numWindow.getText().toString()) * 60 * 1000;
    }

    private void updateLocationDisplay() {
        if (lastLocation == null) {
            lblCurLocation.setText("");
            lblCurLocation.setVisibility(View.INVISIBLE);
            barLoading.setVisibility(View.VISIBLE);
        } else {
            lblCurLocation.setText(String.format("Lat: %f, Lon: %f",
                    lastLocation.getLatitude(), lastLocation.getLongitude()));
            lblCurLocation.setVisibility(View.VISIBLE);
            barLoading.setVisibility(View.GONE);
        }
    }

    private void addFriend(){
        System.out.println("Creating popup...");
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View root = inflater.inflate(R.layout.add_friend, null);
        root.setVisibility(View.VISIBLE);
        ListView emails = (ListView) root.findViewById(R.id.lstFriendEmails);
        final FriendEmailAdapter adapter = new FriendEmailAdapter(getApplicationContext());
        emails.setAdapter(adapter);
        Button btnDone = (Button) root.findViewById(R.id.btnDone);

        popupWindow = new PopupWindow(root,
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT, true);
        popupWindow.showAtLocation(lblCurLocation, Gravity.CENTER, 0, 0);
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                friends.addAll(adapter.getEmails());
                new AddFriendsTask().execute(friends.toArray(new String[friends.size()]));
            }
        });
    }

    private void saveLocation(){
        if(storeLocationTask == null) {
            storeLocationTask = new StoreLocationTask();
            storeLocationTask.execute(lastLocation);
        }
    }

    private void refreshFriends(){
        if(getFriendsTask == null) {
            getFriendsTask = new GetFriendsTask();
            getFriendsTask.execute();
        }else{
            Toast.makeText(getApplicationContext(),
                    "That task is already running!", Toast.LENGTH_SHORT).show();
        }
    }

    private void setUpLocationManager(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        }else {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime, minDistance, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    lastLocation = location;
                    timeUpdated = System.currentTimeMillis();
                    updateLocationDisplay();
                    saveLocation();
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            });
            lastLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            updateLocationDisplay();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        assert requestCode == REQUEST_LOCATION;
        if(grantResults[0] != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getApplicationContext(),
                    "You know we need your location, right?", Toast.LENGTH_LONG).show();
        }else{
            setUpLocationManager();
        }
    }

    private void recieveFriendUpdate(FriendLocation friendLocation){
        if(System.currentTimeMillis() - friendLocation.time > getTimingWindow()){
            return;
        }
        double myLat = lastLocation.getLatitude();
        double myLon = lastLocation.getLongitude();
        float[] results = new float[1];
        Location.distanceBetween(myLat, myLon, friendLocation.latitude, friendLocation.longitiude, results);
        double miles = results[0] * 0.000621371;
        if(miles <= getRadius()){
            adapter.add(friendLocation);
        }
    }

    private boolean checkResponse(JSONObject response, String... keys){
        if(response == null){
            Toast.makeText(getApplicationContext(),
                    "Problem saving!", Toast.LENGTH_LONG).show();
        }else{
            for(String key : keys) {
                try {
                    String status = response.getString(key);
                    if (!status.equals("updated") && !status.equals("created")) {
                        Toast.makeText(getApplicationContext(),
                                "Problem saving!", Toast.LENGTH_LONG).show();
                        System.err.println("Key: \"" + key + "\", Status: \"" + status + '"');
                    }else{
                        return true;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    System.err.println(response.toString());
                    Toast.makeText(getApplicationContext(),
                            "Key not found!", Toast.LENGTH_LONG).show();
                }
            }
        }
        return false;
    }

    private class StoreLocationTask extends AsyncTask<Location, Void, JSONObject>{

        String key = null;

        @Override
        protected JSONObject doInBackground(Location... params) {
            String token = adminToken;
            assert token != null;
            try {
                key = getLocationKey();
            }catch(NullPointerException e){
                return null;
            }
            JSONObject loc = new JSONObject();
            JSONObject data = new JSONObject();
            try {
                loc.put("user", getUsername());
                loc.put("lat", params[0].getLatitude());
                loc.put("lon", params[0].getLongitude());
                loc.put("time", timeUpdated);
                data.put(key, loc);
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }

            ObjectSet choreo = new ObjectSet(TEMBOO_SESSION);
            ObjectSet.ObjectSetInputSet inputSet = choreo.newInputSet();

            inputSet.set_APIKey(cloudmineAPI);
            inputSet.set_ApplicationIdentifier(cloudmineAppID);
            inputSet.set_SessionToken(token);
            inputSet.set_Data(data.toString());

            try {
                return new JSONObject(choreo.execute(inputSet).get_Response());
            } catch (JSONException | TembooException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject response) {
            if(response != null) {
                try {
                    checkResponse(response.getJSONObject("success"), key);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class GetFriendsTask extends AsyncTask<Void, String, JSONObject>{

        String friendsListKey = null;

        private JSONObject get(String keys, String token) throws TembooException, JSONException {
            System.out.println("Get: "+keys);
            assert token != null;
            ObjectGet get = new ObjectGet(TEMBOO_SESSION);
            ObjectGet.ObjectGetInputSet inputSet = get.newInputSet();

            inputSet.set_APIKey(cloudmineAPI);
            inputSet.set_ApplicationIdentifier(cloudmineAppID);
            inputSet.set_SessionToken(token);
            inputSet.set_Keys(keys);

            return new JSONObject(get.execute(inputSet).get_Response());
        }

        @Override
        protected void onProgressUpdate(String... values) {
            Toast.makeText(getApplicationContext(),
                    values[0], Toast.LENGTH_LONG).show();
        }

        @Override
        protected JSONObject doInBackground(Void... params) {
            String username = getUsername();
            assert username != null;
            friendsListKey = getFriendsKey();
            if(friends == null || friends.isEmpty()){
                try {
                    JSONObject response = get(friendsListKey, getUserToken());
                    JSONArray friendsJSON = response.getJSONObject("success").getJSONArray(friendsListKey);
                    friends = new ArrayList<>(friendsJSON.length());
                    for(int i=0; i<friendsJSON.length(); i++){
                        friends.add(friendsJSON.getString(i));
                    }
                } catch (JSONException | TembooException e) {
                    e.printStackTrace();
                }
            }
            if(friends == null || friends.isEmpty()){
                System.out.println("User has no friends");
                publishProgress("You have no registered friends!");
                return null; //Well, I tried.
            }
            List<String> friendsCopy = new ArrayList<>(friends);
            StringBuilder keys = new StringBuilder();
            for(int i=0; i<friendsCopy.size(); i++){
                if(i > 0){
                    keys.append(',');
                }
                keys.append(getLocationKey(friendsCopy.get(i)));
            }
            try {
                return get(keys.toString(), adminToken).getJSONObject("success");
            } catch (TembooException | JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            if(result == null){
                getFriendsTask = null;
                return;
            }
            String key;
            Iterator<String> keys = result.keys();
            while(keys.hasNext()){
                key = keys.next();
                try {
                    JSONObject loc = result.getJSONObject(key);
                    recieveFriendUpdate(new FriendLocation(loc.getString("user"),
                            loc.getDouble("lat"),
                            loc.getDouble("lon"),
                            loc.getLong("time")));
                } catch (JSONException e) {
                    e.printStackTrace();
                    System.err.println("result = "+result.toString());
                }
            }
            getFriendsTask = null;
        }
    }

    private class AddFriendsTask extends AsyncTask<String, Void, JSONObject>{

        String key;

        @Override
        protected void onPreExecute() {
            btnAddFriend.setEnabled(false);
        }

        @Override
        protected JSONObject doInBackground(String... names) {
            String token = getUserToken(), username = getUsername();
            assert token != null;
            assert username != null;
            key = getFriendsKey();
            JSONArray array = new JSONArray();
            for(String name : names){
                array.put(name);
            }
            JSONObject data = new JSONObject();
            try {
                data.put(key, array);
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
            System.out.println("Friends data: "+data.toString());
            ObjectSet choreo = new ObjectSet(TEMBOO_SESSION);
            ObjectSet.ObjectSetInputSet inputSet = choreo.newInputSet();

            inputSet.set_APIKey(cloudmineAPI);
            inputSet.set_ApplicationIdentifier(cloudmineAppID);
            inputSet.set_SessionToken(token);
            inputSet.set_Data(data.toString());

            try {
                return new JSONObject(choreo.execute(inputSet).get_Response());
            } catch (JSONException | TembooException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(JSONObject response) {
            btnAddFriend.setEnabled(true);
            if(response == null){
                Toast.makeText(getApplicationContext(),
                        "There was a problem adding friends!", Toast.LENGTH_LONG).show();
            }else{
                try {
                    if(checkResponse(response.getJSONObject("success"), key)){
                        Toast.makeText(getApplicationContext(),
                                "Friend added!", Toast.LENGTH_SHORT).show();
                    }else{
                        System.err.println(response.toString());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    System.err.println(response.toString());
                }
            }
        }
    }

    private class AdminLoginTask extends AsyncTask<Void, String, String>{

        @Override
        protected void onProgressUpdate(String... values) {
            Toast.makeText(getApplicationContext(),
                    values[0], Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                JSONObject response = LoginActivity.login("admin", "as;oasfjngfaskjfnjvnei3qu783893ijfnfaihs");
                publishProgress("Admin connected");
                return response.getString("session_token");
            } catch (TembooException | JSONException e) {
                e.printStackTrace();
                publishProgress("Failed to connect to server");
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if(s != null){
                adminToken = s;
            }
        }
    }
}
