package thighfill.a2;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private List<FriendLocation> locations = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Intent intent = getIntent();
        double[] longitudes = intent.getDoubleArrayExtra(MainActivity.EXTRA_LONGITUDES);
        double[] latitudes = intent.getDoubleArrayExtra(MainActivity.EXTRA_LATITUDES);
        String[] names = intent.getStringArrayExtra(MainActivity.EXTRA_NAMES);
        long[] times = intent.getLongArrayExtra(MainActivity.EXTRA_TIMES);
        assert longitudes.length == latitudes.length && names.length == longitudes.length;

        locations = new ArrayList<>(names.length);
        for(int i=0; i<names.length; i++){
            locations.add(new FriendLocation(names[i],latitudes[i],longitudes[i], times[i]));
        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        for(int i=0; i<locations.size(); i++){
            FriendLocation loc = locations.get(i);
            LatLng latLng = new LatLng(loc.latitude, loc.longitiude);
            Date date = new Date(loc.time);
            mMap.addMarker(new MarkerOptions().position(latLng).title(loc.username).snippet(date.toString()));
            System.out.println("Mapped "+latLng.toString());
            if(i == 0){
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17.5f));
            }
        }
    }
}
