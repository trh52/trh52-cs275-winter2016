package thighfill.a2;

/**
 * @author Tobias Highfill
 */
public class FriendLocation {
    public String username = null;
    public double latitude = 0;
    public double longitiude = 0;
    public long time = -1;

    public FriendLocation(String username, double latitude, double longitiude, long time){
        this.username = username;
        this.latitude = latitude;
        this. longitiude = longitiude;
        this.time = time;
    }
}
