CS 275 Assignment 2 - Who's Around?
=====

By: Tobias Highfill (trh52)

This one was a little tricky because I had to figure out Cloudmine and there
were a couple of things I was not expecting. For example, User objects share
a namespace but they cannot be accessed by others.

The location data for all users is stored under a special admin account with
keys like:

```javascript
"example_email_com_location" = {
    "user":"example@email.com", //User's email
    "lat":38.5,                 //Latitude
    "lon":-75.6,                //Longitude
    "time":142394572392394      //UNIX timestamp
}
```

The user's friends are stored in their own account like:

```javascript
"example_email_com_friends" = ["other@email.com", "some@email.com"]
```

This is all fetched through Temboo.